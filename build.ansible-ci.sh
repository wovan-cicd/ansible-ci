#!/bin/bash

. ./.env

if [ "${CI_REGISTRY_IMAGE}" == "" ]; then
    CI_REGISTRY_IMAGE=ansible-ci
fi

# [py-version-tag]=py-image:version
# [py2]=python:2.7-slim
declare -A DI_TAGS=(    
    [py3]=python:3.7-slim
)

for DI_TAG in "${!DI_TAGS[@]}"
do
    for ANSIBLE_VERSION in ${ANSIBLE_VERSIONS_SUPPORTED[*]}
    do
      image_name="${CI_REGISTRY_IMAGE}:${DI_TAG}-${ANSIBLE_VERSION}"
      echo -e "\n---------- [${image_name}] ----------\n"

      docker build -f ansible-ci.Dockerfile --pull \
        --build-arg="ANSIBLE_VERSION=${ANSIBLE_VERSION}" \
        --build-arg="DI_PYTHON=${DI_TAGS[$DI_TAG]}" \
        -t "${image_name}" .      
      docker push "${image_name}"
    done
done
