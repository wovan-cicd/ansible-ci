ARG DI_ANSIBLE="ansible-ci:py2-2.7.11"
FROM ${DI_ANSIBLE}

LABEL "com.example.maintainer"="https://gitlab.com/wOvAN"

### DockerCLI

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
  && add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/debian \
  $(lsb_release -cs) \
  stable" \
  && apt-get update \
  && apt-get install -y docker-ce-cli \
  && apt-get clean

ARG KUBESPRAY_VERSION
RUN git clone https://github.com/kubernetes-sigs/kubespray.git \
  && cd kubespray && git checkout ${KUBESPRAY_VERSION} && git branch && cd .. \
  && pip install -r ./kubespray/requirements.txt \
  && pip install -r ./kubespray/contrib/inventory_builder/requirements.txt \
  && pip install -r ./kubespray/contrib/dind/requirements.txt \
  && pip install -r ./kubespray/contrib/vault/requirements.txt \
  && pip install -r ./kubespray/contrib/network-storage/heketi/requirements.txt \
  && rm -rf ./kubespray

### Install Kubectl

ARG KUBECTL_VERSION
RUN set -ex \    
  && curl -sSL https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
  && chmod -v +x /usr/local/bin/kubectl

### Install Helms

ARG HELM_VERSION
RUN set -ex \    
  && curl -sSL https://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar xz \
  && mv linux-amd64/helm /usr/local/bin/helm \
  && chmod -v +x /usr/local/bin/helm \
  && rm -rf linux-amd64

### Init Helm client
# RUN helm init --client-only
RUN echo "\e[1m\e[33m -> Python version:  $(python --version 2>&1)\e[0m"
RUN echo "\e[1m\e[33m -> Ansible version: $(ansible-playbook --version | head -n1 | awk '{print $2}')\e[0m"
RUN echo "\e[1m\e[33m -> Kubectl version: $(kubectl version --client)\e[0m"
RUN echo "\e[1m\e[33m -> Helm version:    $(helm version --client)\e[0m"



