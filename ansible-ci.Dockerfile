ARG DI_PYTHON="python:2.7-slim"
FROM ${DI_PYTHON}

LABEL "com.example.maintainer"="https://gitlab.com/wOvAN"

ENV DEBIAN_FRONTEND=noninteractive

### Install OS Updates and Packages

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
    libssl-dev \
    python-dev \
    software-properties-common \ 
    apt-transport-https \
    build-essential \
    bash \
    gnupg \
    gnupg2 \
    curl \
    wget \
    gcc \
    sshpass \
    openssh-client \
    git \
    rsync \
    ca-certificates \
    make \
    jq \
    nmap \
    uuid-runtime \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/*

### DockerCLI

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
  && add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/debian \
  $(lsb_release -cs) \
  stable" \
  && apt-get update \
  && apt-get install -y docker-ce-cli \
  && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/*

### Docker-Compose

RUN dcLatestVer=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4) \ 
  && echo "Latest version: [${dcLatestVer}]" \
  && curl -L https://github.com/docker/compose/releases/download/${dcLatestVer}/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose \
  && chmod +x /usr/local/bin/docker-compose \
  && echo "Installed version: [$(docker-compose --version)]"

### Update PIP

RUN pip install --no-cache -U pip

### Install Ansible

ARG ANSIBLE_VERSION
COPY /configs/hosts /etc/ansible/hosts
COPY /configs/ansible.cfg /etc/ansible/ansible.cfg
ENV ANSIBLE_CONFIG /etc/ansible/ansible.cfg
COPY requirements.txt requirements.txt
RUN pip install --no-cache -r requirements.txt

### Default Vars

ENV ANSIBLE_HOST_KEY_CHECKING false
ENV ANSIBLE_FORCE_COLOR true

RUN ansible-playbook --version

### Default command: display Ansible version

CMD [ "ansible-playbook", "--version" ]
