#!/bin/bash

. ./.env

if [ "${CI_REGISTRY_IMAGE}" == "" ]; then
    CI_REGISTRY_IMAGE=ansible-ci
fi

DI_TAG=kubespray

image_name="${CI_REGISTRY_IMAGE}:${DI_TAG}-${KUBESPRAY_VERSION}"
echo -e "\n---------- [${image_name}] ----------\n"

docker build -f ${DI_TAG}.Dockerfile --pull \
    --build-arg="KUBESPRAY_ANSIBLE_VERSION=${KUBESPRAY_ANSIBLE_VERSION}" \
    --build-arg="DI_ANSIBLE=${CI_REGISTRY_IMAGE}:py2-${KUBESPRAY_ANSIBLE_VERSION}" \
    --build-arg="KUBESPRAY_VERSION=${KUBESPRAY_VERSION}"  \
    --build-arg="KUBECTL_VERSION=${KUBECTL_VERSION}" \
    --build-arg="HELM_VERSION=${HELM_VERSION}" \
    -t "${image_name}" .
docker push "${image_name}"


